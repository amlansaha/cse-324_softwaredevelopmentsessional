import os,time
import subprocess
from sys import argv

#call(["head","-10","Temporary.cpp","|","tail","-2","Temporary.cpp"])
crd=os.getcwd()
files = os.listdir(crd)

for filename in files:
	if(os.path.isfile(filename)==False):
		continue
#	filename = argv[1]
	templist = filename.split('.')
	file_domain = templist[0]
	ext = templist[1]
	if(ext != 'c' ):
		continue

	compile_log_file = open(crd+'/cmplog.'+file_domain,'w')
	error_file = open(crd+'/err.'+file_domain,'w')

	verdict = "CMP"
	compile_status = subprocess.call("gcc "+filename+" -w",stdout=compile_log_file,stdin=None,shell=True,stderr=error_file)

	compile_log_file.close()
	error_file.close()

	os.remove(crd+'/cmplog.'+file_domain)
	if (compile_status==0):
		os.remove(crd+'/err.'+file_domain)

	#p = subprocess.Popen("gcc "+filename+" -w",stdout=open(crd+'/out.'+file_domain,'w'),stdin=None,shell=True,stderr=open(crd+'/err.'+file_domain,'w'))


	ts = 0
	te = -1
	verdict = 'RN'

	try:
		if (compile_status==0):
			file_domain = 'testing'
			input_file = open(os.path.join(crd,file_domain+".1.in"),'r')
			output_file = open(os.path.join(crd,file_domain+'.1.out'),'w')
			original_output = "original.1.out"
		
			ts = time.time()
			running_status = subprocess.call("./a.out",stdin=input_file,stdout=output_file,timeout=2)
			te = time.time()
		
			input_file.close()
			output_file.close()
	#		original_output.close()
		
	#		print("Returned "+str(pp))	#returns -11 for array out of index

			if (running_status < 0 ):
				verdict = "RE"	#RUNTIME ERROR
	#			print("RUNTIME ERROR\n")
			else:
				out_diff = open('output_difference_'+file_domain,'w')
				check = subprocess.call('diff '+original_output + ' '+file_domain+'.1.out', stdout = out_diff,shell=True)
	#			print("diff output: ",check)
				if ( check == 0):
					verdict = "AC"
				else:
					verdict = "WA"
				out_diff.close()
				os.remove('output_difference_'+file_domain)
		else:
			verdict = "CE"
	
	except subprocess.TimeoutExpired as ex:
		print(ex.timeout)
	#	print("timeout... returned: ",pp)
		te = time.time()
	#	print("Returned "+str(pp))	#returns -11 for array out of index
		verdict = "TLE"
	
	running_time = te-ts	
	print("Filename:",filename)
	print("Total running time: ",running_time)
	print("Verdict:",verdict)
