///*
//prob: BlinkTest
//id: amlansaha
//lang: C++
//date: Nov 23, 2015
//algo:
//*/
//
//#include<avr/io.h>
//#include<util/delay.h>
//
//int main(void)
//{
////	DDRA = 0;
//	MCUCSR = 1<<JTD;
//	MCUCSR = 1<<JTD;
//	DDRA = 0x00;
//	DDRB = 0x00;
//	DDRC = 0x00;
////	DDRD = 0b01000000;
//	PORTA = 0xFF;
//	PORTB = 0x00;
//	PORTC = 0x00;
//
//	while(1)	{
//		int i,j,k;
//		PORTC = 0x05;
//		for ( i = 0; i<8; i++ )	{
//			PORTC = i;
//			_delay_ms(400);
//		}
//	}
//	while(1)	{
//		PORTA = 0x01;
//		PORTB = 0x00;
//		int i,j,k;
////		for ( PORTC = 0; PORTC < 8; PORTC++ )	{
//			PORTA = 0x00;
//			for (i=0;i<8;i++)	{
//				PORTA = PORTA<<i;
//				_delay_ms(1000);
//			}
//			PORTA = 0x00;
//			PORTB = 0x01;
//			for (i=0;i<8;i++)	{
//				PORTB = PORTB<<i;
//				_delay_ms(1000);
//			}
////		}
//		PORTC = ~(PORTC);
//	}
//	return 0;
//}
////mosi
////misc
////sck
////res
////vcc
////gnd


/*
prob: Snake
*/

#include<avr/io.h>
#include<avr/interrupt.h>
#include<util/delay.h>
#include <stdlib.h>
#include<stdio.h>

#define Abs(x) ((x)<0)? ((x)*-1) : (x)
#define Max(x,y) ((x)>(y)) ? (x) : (y)

int food_locX, food_locY;
int LEVEL;
int OVERFLOW_COUNT;
int DIRECTION;
int game_status;

#define NOTHING 0
#define UP 1
#define LEFT 2
#define RIGHT 3
#define DOWN 4

#define SPACE 0
#define BODY 1
#define FOOD 2
#define BLOCK 3
#define HEAD 4
#define TAIL 5

#define MAX_ROW 16
#define MAX_COL 16

#define NOT_STARTED 0
#define RUNNING 1
#define PAUSED 2
#define GAME_OVER 3

int LEVEL;
int SNAKE_LENGTH;
int HEADX, HEADY, TAILX, TAILY;

int8_t input()
{
	int8_t ch = PIND;
	ch = ch & (0xFF);
	return DIRECTION;
}

int8_t X[] = {0,0,-1,1,0};
int8_t Y[] = {0,-1,0,0,1};

int8_t MOD(int8_t x)
{
	return ((x%16)+16)%16;
}

int8_t ARENA[16][16] = {
		0,0,0,0,0,0,0,0,	0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,	0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,	0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,	0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,FOOD,	0,0,0,0,3,3,3,3,
		0,0,0,0,0,0,0,0,	0,0,0,0,3,0,0,0,
		0,0,0,0,0,0,0,0,	0,0,0,0,3,3,3,3,
		0,0,0,0,0,0,0,0,	0,0,0,0,0,0,0,0,

		0,0,0,0,0,0,0,0,	0,0,0,0,0,0,0,0,
		0,0,0,0,TAIL,BODY,BODY,HEAD,	0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,	0,0,0,0,0,0,0,0,
		3,3,3,3,0,0,0,0,	0,0,0,0,0,0,0,0,
		0,0,0,3,0,0,0,0,	0,0,0,0,0,0,0,0,
		3,3,3,3,0,0,0,0,	0,0,0,0,0,0,0,0,
		0,0,0,0,0,BLOCK,BLOCK,	BLOCK,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,	0,0,0,0,0,0,0,0
};

int8_t next_cell[16][16], prev_state[16][16];

void drawArena(int8_t arena[][16]);

void initialize_game(int8_t arena[][16]);

void move_snake(int8_t arena[][16]);

ISR(TIMER1_OVF_vect)
{
	OVERFLOW_COUNT++;
	if ( OVERFLOW_COUNT == 8 )	{
		if ( game_status == RUNNING)
			move_snake(ARENA);
		else if ( game_status == PAUSED )	{}
		else if ( game_status == GAME_OVER )	{

		}
		OVERFLOW_COUNT = 0;
	}
}

int main(void)
{
//	DDRA = 0;
	MCUCSR = 1<<JTD;
	MCUCSR = 1<<JTD;
	DDRA = 0xFF;
	DDRB = 0xFF;
	DDRC = 0xFF;
	DDRD = 0xF0;

	OVERFLOW_COUNT = 0;
	//configure timer
	TCCR1A = 0x00;
	TCCR1B = 0x01;
	//Enable Overflow Interrupt
	TIMSK = 0x04;

//	PORTD = 0b00001010;
	game_status = NOT_STARTED;
	while(!(PIND & 0x0F))	{
		//waiting to start
	}
	initialize_game(ARENA);
	game_status = RUNNING;
	sei();
	drawArena(ARENA);
	cli();
//	while(1)	{
////		PORTA = ~(PORTA);
//		unsigned char i,j;
//
//		for ( i = 0; i < 16; i++ )	{
////			for ( j = 0; j < 200; j++ )	{
//				PORTD = i;
//				PORTA = 0xFF;
//				PORTB = 0x00;
//				_delay_ms(.25);
//				PORTA = 0x00;
//				PORTB = 0xFF;
//				_delay_ms(.25);
////			}
//		}
//			for ( j = 0; j < 8; j++ )	{
//				PORTA = PORTA<<j;
//				_delay_ms(500);
//			}
//			PORTA = 0x00;
//			PORTB = 0x01;
//			for ( j = 0; j < 8; j++ )	{
//				PORTB = PORTB<<j;
//				_delay_ms(500);
//			}
//			PORTA = ~(PORTA);
////			PORTB = ~(PORTB);
//			_delay_ms(500);
//
//			PORTA = ~(PORTA);
////			PORTB = ~(PORTB);
//			_delay_ms(500);
////			for ( j = 0; j < 8; j++ )	{
////				PORTA = 1<<j;
////				_delay_ms(500);
////			}
//	}
	return 0;
}


void drawArena(int8_t arena[][16])
{
	int8_t col_port_A,col_port_B,col,i,j,k,l,row;
	while(1)	{
		for(col=0;col<MAX_COL; col++)	{
			col_port_A = 0;
			col_port_B = 0;
			PORTC = col;

			for ( row = 0; row < 8; row++ )	{
				switch(arena[row][col])	{
					i = row;
					case SPACE:
						break;
					case BODY:
						col_port_A = col_port_A | (1<<row);
						break;
					case BLOCK:
						col_port_A = col_port_A | (1<<row);
						break;
					case FOOD:
						col_port_A = col_port_A | (1<<row);
						break;
					case HEAD:
						col_port_B = col_port_B | (1<<i);
						break;
					case TAIL:
						col_port_B = col_port_B | (1<<i);
						break;
				}
			}

			for ( row = 8; row < 16; row++ )	{
				i = row-8;
				switch(arena[row][col])	{
					case SPACE:
						break;
					case BODY:
						col_port_B = col_port_B | (1<<i);
						break;
					case BLOCK:
						col_port_B = col_port_B | (1<<i);
						break;
					case FOOD:
						col_port_B = col_port_B | (1<<i);
						break;
					case HEAD:
						col_port_B = col_port_B | (1<<i);
						break;
					case TAIL:
						col_port_B = col_port_B | (1<<i);
						break;
				}
			}
			PORTA = col_port_A;
			PORTB = 0x00;
			_delay_ms(.1);
			PORTB = col_port_B;
			PORTA = 0x00;
			_delay_ms(.1);
		}
	}
}

void initialize_game(int8_t arena[16][16])
{
	int i,j;
	HEADX = 0;
	HEADY = 0;
	TAILX = MAX_COL;
	TAILY = MAX_ROW;
	DIRECTION = RIGHT;
	SNAKE_LENGTH = 0;

	for ( i = 0; i < MAX_ROW; i++ )	{
		for ( j = 0; j < MAX_COL; j++ )	{
			if ( arena[i][j] == HEAD )	{
				HEADX = i;
				HEADY = j;
				SNAKE_LENGTH++;
			}
			else if ( arena[i][j] == TAIL )	{
				TAILX = i;
				TAILY = j;
				SNAKE_LENGTH++;
			}
			else if ( arena[i][j] == BODY)	SNAKE_LENGTH++;
			next_cell[i][j] = 0;
		}
	}
	int crntx = TAILX, crnty = TAILY, tempx,tempy;

	for ( i = 0; i < SNAKE_LENGTH; i++ )	{
		if ( arena[crntx][crnty] == HEAD )	{
			next_cell[crntx][crnty] = DIRECTION;
			continue;
		}
		for ( j = 1; j <= 4; j++ )	{
			tempx = MOD(crntx+X[j]);
			tempy = MOD(crnty+Y[j]);
			if ( arena[tempx][tempy] == BODY || arena[tempx][tempy] == TAIL )	{
				if ( next_cell[tempx][tempy] )	continue;
				next_cell[crntx][crnty] = j;
				crntx = tempx;
				crnty = tempy;
				break;
			}
		}
	}

}

void move_snake(int8_t arena[][16])
{
	int i,j,newx,newy,dir;
	unsigned char food_eaten = 0;

	for ( i = 0; i < MAX_ROW; i++ )	{
		for ( j = 0; j < MAX_COL; j++ )	{
			if ( arena[i][j] == HEAD )	{
				dir = next_cell[i][j];
				newx = MOD(i+X[dir]);
				newy = MOD(j+Y[dir]);
				if ( arena[i][j] == HEAD )	{
					if ( Abs(arena[newx][newy]) == FOOD || arena[newx][newy] == SPACE )	{
						if( Abs(arena[newx][newy]) == FOOD )	{
							SNAKE_LENGTH++;
							food_eaten = 1;
						}
						arena[newx][newy] = HEAD;
						next_cell[newx][newy] = DIRECTION;
						arena[i][j] = BODY;
						HEADX = newx;
						HEADY = newy;
					}
					else	{
						game_status = GAME_OVER;
					}
				}
//				else	{
//					next_cell[newx][newy] = DIRECTION;
//				}
			}
			if ( Abs(arena[i][j]) == FOOD )	arena[i][j]*=-1;
		}
	}

	if ( !food_eaten )	{
		arena[TAILX][TAILY] = SPACE;
		dir = next_cell[TAILX][TAILY];
		TAILX = TAILX+X[dir];
		TAILY = TAILY+Y[dir];
	}
}


