from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.home, name='home'),
    url(r'^home$', views.home, name='home'),
    url(r'^register$', views.user_register, name='user_register'),
#    url(r'^([^\_]+[_][^\_]+)/(?P<source>[^\_]+)[_](?P<dest>[^\_]+)[_](?P<option>[^\_]+)/$', views.retrieve_details, name='retrieve_details'),
    #user auth urls
    url(r'^dashboard', views.dashboard),
    url(r'^login(/(?P<redir>.+)?)?', views.user_login),
#    url(r'^login/.+/', views.user_login),
    url(r'^logout$', views.user_logout),
    
    url(r'^post_problem/?',views.post_problem),
    url(r'^post_solution/?',views.post_solution),
#    url(r'^post_testcases/?',views.post_testcases),
    url(r'^problemset(/(?P<prob_id>.*)?)?',views.show_problemset),
    url(r'^problems(/cat=(?P<category_tag>[^/]+)?)?(/pageno=(?P<pageno>\d*)?)?',views.show_problems),
    url(r'^submit(/(?P<prob_id>.*)?)?',views.submit_soln),
    url(r'^submissions(/user=(?P<username>[^/]+)?)?(/pageno=(?P<pageno>\d*)?)?',views.view_submissions),
    
    url(r'^ranklist',views.show_ranklist),
    url(r'^categories',views.show_categories),
    
    url(r'^add_city',views.add_city),
    url(r'^add_country',views.add_country),
    url(r'^add_institution',views.add_institution),
    url(r'^add_language',views.add_language),
#    url(r'^login/(?P<email>[^\_]+)[_](?P<pw>[^\_]+)$', views.user_login),
#    url(r'^loggedin/$', views.user_loggedin),
#    url(r'^invalid/$', views.user.invalid),
]


