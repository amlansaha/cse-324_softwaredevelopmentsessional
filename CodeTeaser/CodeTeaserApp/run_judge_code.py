import os,time
#import models
from CodeTeaserApp.models import *
from django.conf import settings
#from django.core.files.base import ContentFile
from django.core.files import File
import subprocess
from sys import argv

#call(["head","-10","Temporary.cpp","|","tail","-2","Temporary.cpp"])

#This class will run the original solution code posted by the problemsetter against the provided test files
class JudgeCoderunner(object):
	MEDIA_ROOT = ""
	filepath=''
	filedir=''
	filename = ''
	vtype = {}
	soln = None
	
	def __init__(self,soln):
		crd=os.getcwd()
		os.chdir(settings.MEDIA_ROOT)

		self.MEDIA_ROOT = settings.MEDIA_ROOT
	#	print(filepath)
#		self.filedir = os.path.dirname(self.filepath)
		self.filename = self.MEDIA_ROOT + '/' + soln.code.name #os.path.split(self.filepath)[1]
		self.filepath = os.path.split(self.filename)[0]
		
		self.soln = soln
		
#		os.chdir(self.filepath)
		
		tempname = os.path.split(self.filename)[-1]
		self.file_domain = tempname.split('.')[0]
		
		self.compile_status = self.compile_code(soln)
		if ( self.compile_status != 0 ):	#failed to compile
			return

		self.run_code(soln)
	
		os.chdir(crd)
	
	def compile_code(self,soln):
		self.compile_log_file = open(self.filepath+'/cmplog.'+self.file_domain,'w')
		self.error_file = open(self.filepath+'/err.'+self.file_domain,'w')
		print("Compilling...",soln.problem)
#		soln.setVerd(self.vtype["CMP"])
#		soln.save()
		
	
		compile_status = 1
		
		if soln.language.lang == 'C':
			compile_status = self.compile_c_code()
		elif soln.language.lang == 'C++':
			compile_status = self.compile_cpp_code()
		elif soln.language.lang == 'C++11':
			compile_status = self.compile_cpp11_code()
		elif soln.language.lang.lower() == 'java':
			compile_status = self.compile_java_code()

		self.compile_log_file.close()
		self.error_file.close()

#		os.remove('cmplog.'+self.file_domain)
#		os.remove('err.'+self.file_domain)
		
		print("Compiled...status: ",compile_status) 
		
		return compile_status
	
	
	def compile_c_code(self):
		compile_status = subprocess.call("gcc "+self.filename+" -w -std=c99 -o "+self.filepath+'/a.out',stdout=self.compile_log_file,stdin=None,shell=True,stderr=self.error_file)
		return compile_status
	
	def compile_cpp_code(self):
		compile_status = subprocess.call("g++ "+self.filename+" -w -std=c99 -o "+self.filepath+'/a.out',stdout=self.compile_log_file,stdin=None,shell=True,stderr=self.error_file)
		return compile_status
	
	def compile_cpp11_code(self):
		compile_status = subprocess.call("g++ "+self.filename+" -w -std=c++11 -o "+self.filepath+'/a.out',stdout=self.compile_log_file,stdin=None,shell=True,stderr=self.error_file)
		return compile_status
	
	def compile_java_code(self):
		compile_status = subprocess.call("javac -d "+self.filepath+' '+self.filename,stdout=self.compile_log_file,stdin=None,shell=True,stderr=self.error_file)
#		print("java compile status: ",compile_status)
		return compile_status
		
		
		
		
	def run_code(self,soln):
		ts = 0
		te = -1
		running_time = 0.0
		
#		soln.save()
		
		problem_id = soln.problem.problem_id
		
		testcase_inputdir = os.path.join(self.MEDIA_ROOT,'Inputs',problem_id)
		testcase_outdir = os.path.join(self.MEDIA_ROOT,'Outputs',problem_id)
		relative_path = os.path.join('Outputs',problem_id)
		
		testcases = TestCase.objects.filter(problem = soln.problem)
		
		for test in testcases:
			caseno = test.caseno
			input_file = open(os.path.join(settings.MEDIA_ROOT,test.inp.name),'r')
			out_file_name = problem_id+'.'+str(caseno)+'.out'	#the file name of the output file
			output_file_dir = os.path.join(testcase_outdir,out_file_name)	#the directory of the output file
			if(not os.path.exists(testcase_outdir)):
				os.makedirs(testcase_outdir)
			output_file = open(output_file_dir,'w+')
			print("outdir: ",testcase_outdir, " output ", output_file_dir)
		
			ts = time()
			time_limit = soln.problem.time_limit/1000.0	#Time limit is saved as miliseconds in the database. Here we have to convert it to seconds.
			running_status = ''
			
			if soln.language.lang == 'C' or soln.language.lang == 'C++' or soln.language.lang == 'C++11':
				running_status = self.run_c_binary_file(inp = input_file,outp = output_file,time_limit=time_limit)
			elif soln.language.lang.lower() == 'java':
				running_status = self.run_java_class(inp = input_file,outp = output_file,time_limit=time_limit)
				
			te = time()
			self.running_time = max(running_time,te-ts)
			
			input_file.close()

			if (running_status == 'OK' ):
				soln.is_valid = True
			else:
				soln.is_valid = False
				soln.save()
				break
			
			new_file = File(output_file)
			test.outp.save(name=out_file_name,content=new_file)
			test.save()
			
	def run_c_binary_file(self,inp,outp,time_limit):
		try:
#			ts = time()
			path = self.filepath
			running_status = running_status = subprocess.call(path+"/a.out",stdin=inp,stdout=outp,timeout=time_limit)
#			te = time()
			
			if(running_status<0):
				return 'RE'
			
			return 'OK'	
		except subprocess.TimeoutExpired as ex:
			return 'TLE'
		
	def run_java_class(self,inp,outp,time_limit):
		try:
#			ts = time()
			running_status = running_status = subprocess.call("java "+self.filepath+"/Main", shell=True,stdin=inp,stdout=outp,timeout=time_limit)
#			te = time()
			
			if(running_status==0):
				return 'OK'
			
			return 'RE'	
		except subprocess.TimeoutExpired as ex:
			return 'TLE'		
