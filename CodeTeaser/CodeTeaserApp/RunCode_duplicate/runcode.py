from django.models import *
from django.conf import settings

import os,time
import subprocess
from sys import argv

#call(["head","-10","Temporary.cpp","|","tail","-2","Temporary.cpp"])
MEDIA_ROOT = ""
filepath=''
filedir=''
filename = ''

def runcode(submsn):
	crd=os.getcwd()
	global filepath,filedir,filename, MEDIA_ROOT
	filepath = os.path.join(MEDIA_ROOT,submsn.code)
	filedir = os.path.dirname(filepath)
	filename = os.path.split(filepath)[1]
	MEDIA_ROOT = settings.MEDIA_ROOT
	
	os.chdir(filedir)
	
	if submsn.language.lang == 'C':
		run_c_code(submsn)
	elif submsn.language.lang == 'C++':
		run_cpp_code(submsn)
	elif submsn.language.lang == 'C++11':
		run_cpp11_code(submsn)
	
	
def run_c_code(submsn):
	global filename,filepath,filedir
	templist = filename.split('.')
	file_domain = templist[0]
#	if (len(templist)>1):
#		ext = templist[1]
#	else:
#		continue
#		
#	if(ext != 'c' ):
#		continue

	compile_log_file = open(crd+'/cmplog.'+file_domain,'w')
	error_file = open(crd+'/err.'+file_domain,'w')

	submsn.verdict.verd = "CMP"
	compile_status = subprocess.call("gcc "+filename+" -w -std=c99",stdout=compile_log_file,stdin=None,shell=True,stderr=error_file)

	compile_log_file.close()
	error_file.close()

	os.remove(crd+'/cmplog.'+file_domain)
#	if (compile_status==0):
	os.remove(crd+'/err.'+file_domain)

	#p = subprocess.Popen("gcc "+filename+" -w",stdout=open(crd+'/out.'+file_domain,'w'),stdin=None,shell=True,stderr=open(crd+'/err.'+file_domain,'w'))


	ts = 0
	te = -1
	submsn.verdict.verd = 'RN'

	for caseno in range(1,3):
		try:
			if (compile_status==0):
				problem_id = submsn.problem.problem_id
				testcase_inputdir = os.path.join(MEDIA_ROOT,'Inputs')
				testcase_outdir = os.path.join(MEDIA_ROOT,'Outputs')
				
				input_file = open(os.path.join(testcase_inputdir,problem_id+"."+str(caseno)+".in"),'r')
				output_file = open(os.path.join(testcase_outdir,problem_id+"."+str(caseno)+".out"),'w')
				original_output = open(os.path.join(testcase_outdir,problem_id+"."+str(caseno)+".out"))
		
				ts = time.time()
				running_status = subprocess.call("./a.out",stdin=input_file,stdout=output_file,timeout=1)
				te = time.time()
		
				input_file.close()
				output_file.close()
		#		original_output.close()
		
		#		print("Returned "+str(pp))	#returns -11 for array out of index

				if (running_status < 0 ):
					submsn.verdict.verd = "RE"	#RUNTIME ERROR
		#			print("RUNTIME ERROR\n")
					break
				else:
					out_diff = open('output_difference_'+file_domain,'w')
					check = subprocess.call('diff '+original_output + ' '+problem_id+'.'+str(caseno)+'.out', stdout = out_diff,shell=True)
		#			print("diff output: ",check)
					if ( check != 0):
						submsn.verdict.verd = "WA"
						break
					out_diff.close()
	#				print("check::: ",check)
					os.remove('output_difference_'+file_domain)
			else:
				submsn.verdict.verd = "CE"
	
		except subprocess.TimeoutExpired as ex:
	#		print(ex.timeout)
		#	print("timeout... returned: ",pp)
			te = time.time()
		#	print("Returned "+str(pp))	#returns -11 for array out of index
			submsn.verdict.verd = "TLE"
	
	submsn.verdict.running_time = te-ts	
	print("Filename:",filename)
	print("Total running time: ",running_time)
	print("submsn.verdict.verd:",submsn.verdict.verd,"at caseno:",caseno,'\n')
