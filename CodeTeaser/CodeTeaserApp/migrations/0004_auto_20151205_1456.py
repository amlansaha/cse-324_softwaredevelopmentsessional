# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('CodeTeaserApp', '0003_usertype_typename'),
    ]

    operations = [
        migrations.CreateModel(
            name='VerdictType',
            fields=[
                ('vtype', models.CharField(serialize=False, max_length=3, primary_key=True)),
                ('vmsg', models.CharField(unique=True, max_length=22)),
            ],
            options={
                'managed': True,
                'db_table': 'verdict_type',
            },
        ),
        migrations.AlterField(
            model_name='verdict',
            name='verd',
            field=models.ForeignKey(to='CodeTeaserApp.VerdictType', default='IQ'),
        ),
        migrations.AlterUniqueTogether(
            name='verdicttype',
            unique_together=set([('vtype', 'vmsg')]),
        ),
    ]
