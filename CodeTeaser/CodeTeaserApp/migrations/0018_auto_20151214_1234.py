# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('CodeTeaserApp', '0017_auto_20151214_1212'),
    ]

    operations = [
        migrations.RenameField(
            model_name='language',
            old_name='cmd',
            new_name='ext',
        ),
    ]
