# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('CodeTeaserApp', '0014_auto_20151206_0437'),
    ]

    operations = [
        migrations.AlterField(
            model_name='submission',
            name='submission_id',
            field=models.AutoField(serialize=False, primary_key=True),
        ),
    ]
