# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import CodeTeaserApp.models


class Migration(migrations.Migration):

    dependencies = [
        ('CodeTeaserApp', '0028_solution_is_valid'),
    ]

    operations = [
        migrations.AlterField(
            model_name='testcase',
            name='inp',
            field=models.FileField(upload_to=CodeTeaserApp.models.get_input_file_name, null=True),
        ),
        migrations.AlterField(
            model_name='testcase',
            name='outp',
            field=models.FileField(upload_to=CodeTeaserApp.models.get_output_file_name, null=True),
        ),
    ]
