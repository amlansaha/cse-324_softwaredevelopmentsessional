# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('CodeTeaserApp', '0024_problemset_testcase_file'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='testcase',
            unique_together=set([('problem', 'caseno', 'inp', 'outp')]),
        ),
    ]
