# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('CodeTeaserApp', '0012_remove_verdict_submission'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='submission',
            name='id',
        ),
        migrations.AddField(
            model_name='submission',
            name='submission_id',
            field=models.IntegerField(serialize=False, primary_key=True, default=1),
            preserve_default=False,
        ),
    ]
