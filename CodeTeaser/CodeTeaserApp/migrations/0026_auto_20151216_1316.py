# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('CodeTeaserApp', '0025_auto_20151216_1238'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='testcase',
            unique_together=set([('problem', 'caseno')]),
        ),
    ]
