# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('CodeTeaserApp', '0008_auto_20151205_1743'),
    ]

    operations = [
        migrations.AddField(
            model_name='problemset',
            name='is_published',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterUniqueTogether(
            name='solution',
            unique_together=set([('problem', 'language')]),
        ),
    ]
