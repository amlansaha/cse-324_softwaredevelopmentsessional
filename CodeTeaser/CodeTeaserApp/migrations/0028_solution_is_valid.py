# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('CodeTeaserApp', '0027_auto_20151216_1336'),
    ]

    operations = [
        migrations.AddField(
            model_name='solution',
            name='is_valid',
            field=models.BooleanField(default=False),
        ),
    ]
