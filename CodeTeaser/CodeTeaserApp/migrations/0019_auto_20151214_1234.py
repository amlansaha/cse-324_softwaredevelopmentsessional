# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('CodeTeaserApp', '0018_auto_20151214_1234'),
    ]

    operations = [
        migrations.AlterField(
            model_name='language',
            name='ext',
            field=models.CharField(max_length=4),
        ),
    ]
