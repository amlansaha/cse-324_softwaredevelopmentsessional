# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('CodeTeaserApp', '0022_submission_running_time'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='submission',
            name='running_time',
        ),
        migrations.AddField(
            model_name='verdict',
            name='running_time',
            field=models.FloatField(default=0.0),
        ),
    ]
