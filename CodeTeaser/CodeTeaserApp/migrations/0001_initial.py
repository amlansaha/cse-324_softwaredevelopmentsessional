# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings
import datetime


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='City',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('city', models.CharField(max_length=20)),
            ],
            options={
                'db_table': 'city',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='Clarification',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('ask_time', models.DateTimeField(auto_now=True)),
            ],
            options={
                'db_table': 'clarification',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='ClarificationReply',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('ask_time', models.DateTimeField(auto_now=True)),
            ],
            options={
                'db_table': 'clarification_reply',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='Country',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('name', models.CharField(max_length=20, unique=True)),
            ],
            options={
                'db_table': 'country',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='Institution',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('name', models.CharField(max_length=40)),
                ('city', models.ForeignKey(to='CodeTeaserApp.City')),
            ],
            options={
                'db_table': 'institution',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='Language',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('lang', models.CharField(max_length=15, unique=True)),
                ('compiler', models.CharField(max_length=16)),
                ('version', models.CharField(max_length=10)),
                ('cmd', models.CharField(max_length=120)),
            ],
            options={
                'db_table': 'language',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='ProblemCategory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('category_title', models.CharField(max_length=20)),
            ],
            options={
                'db_table': 'problem_category',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='ProblemLock',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
            ],
            options={
                'db_table': 'problem_lock',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='ProblemSet',
            fields=[
                ('problem_id', models.CharField(max_length=12, primary_key=True, serialize=False)),
                ('title', models.CharField(max_length=100)),
                ('statement', models.CharField(max_length=200000)),
                ('constrains', models.CharField(max_length=5120)),
                ('input_format', models.CharField(max_length=5120)),
                ('output_format', models.CharField(max_length=5120)),
                ('sample_input', models.CharField(max_length=5120)),
                ('sample_output', models.CharField(max_length=5120)),
                ('time_limit', models.IntegerField(default=2000)),
                ('mem_limit', models.IntegerField(default=65326)),
            ],
            options={
                'db_table': 'problemset',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='Solution',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('code', models.CharField(max_length=513000)),
                ('language', models.ForeignKey(to='CodeTeaserApp.Language')),
                ('problem_id', models.ForeignKey(to='CodeTeaserApp.ProblemSet')),
            ],
            options={
                'db_table': 'solution',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='Submission',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('code', models.CharField(max_length=513000)),
                ('submission_time', models.DateTimeField(auto_now=True)),
                ('language', models.ForeignKey(to='CodeTeaserApp.Language')),
                ('problem_id', models.ForeignKey(to='CodeTeaserApp.ProblemSet')),
            ],
            options={
                'db_table': 'submission',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='TestCase',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('caseno', models.IntegerField(default=1)),
                ('inp', models.FileField(upload_to='')),
                ('outp', models.FileField(upload_to='')),
                ('problem_id', models.ForeignKey(to='CodeTeaserApp.ProblemSet')),
            ],
            options={
                'db_table': 'testcase',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='Verdict',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('verd', models.CharField(max_length=3, choices=[('AC', 'Accepted'), ('TLE', 'Time Limit Exceeded'), ('WA', 'Wrong Answer'), ('RE', 'Runtime Error'), ('CE', 'Compilation Error'), ('PE', 'Presentation Error'), ('MLE', 'Memory Limit Exceeded'), ('OLE', 'Output Limit Exceeded'), ('IQ', 'In Queue'), ('SF', 'Submission Failed'), ('RN', 'Running')], default='IQ')),
                ('caseno', models.IntegerField(default=1)),
                ('msg', models.CharField(max_length=30)),
                ('submission', models.OneToOneField(related_name='submission_id_for_verdict_table', to='CodeTeaserApp.Submission')),
            ],
            options={
                'db_table': 'verdict',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='Users',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(null=True, blank=True, verbose_name='last login')),
                ('username', models.CharField(max_length=20, unique=True)),
                ('user_full_name', models.CharField(max_length=70)),
                ('user_email', models.EmailField(max_length=255, verbose_name='Email Address', unique=True)),
                ('date_joined', models.DateField(default=datetime.datetime.now)),
                ('is_active', models.BooleanField(default=True)),
                ('is_admin', models.BooleanField(default=False)),
                ('city', models.ForeignKey(null=True, to='CodeTeaserApp.City')),
                ('institution', models.ForeignKey(null=True, to='CodeTeaserApp.Institution')),
            ],
            options={
                'db_table': 'users',
                'managed': True,
            },
        ),
        migrations.AddField(
            model_name='submission',
            name='username',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='submission',
            name='verdict',
            field=models.OneToOneField(related_name='verdict_id_for_submission_table', to='CodeTeaserApp.Verdict'),
        ),
        migrations.AddField(
            model_name='problemset',
            name='authors',
            field=models.ManyToManyField(to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='problemset',
            name='category',
            field=models.ManyToManyField(to='CodeTeaserApp.ProblemCategory'),
        ),
        migrations.AddField(
            model_name='problemlock',
            name='problem_id',
            field=models.ForeignKey(to='CodeTeaserApp.ProblemSet'),
        ),
        migrations.AddField(
            model_name='problemlock',
            name='username',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='clarificationreply',
            name='problem_id',
            field=models.ForeignKey(to='CodeTeaserApp.ProblemSet'),
        ),
        migrations.AddField(
            model_name='clarificationreply',
            name='username',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='clarification',
            name='problem_id',
            field=models.ForeignKey(to='CodeTeaserApp.ProblemSet'),
        ),
        migrations.AddField(
            model_name='clarification',
            name='username',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='city',
            name='country',
            field=models.ForeignKey(to='CodeTeaserApp.Country'),
        ),
        migrations.AlterUniqueTogether(
            name='problemlock',
            unique_together=set([('problem_id', 'username')]),
        ),
        migrations.AlterUniqueTogether(
            name='institution',
            unique_together=set([('name', 'city')]),
        ),
        migrations.AlterUniqueTogether(
            name='city',
            unique_together=set([('city', 'country')]),
        ),
    ]
