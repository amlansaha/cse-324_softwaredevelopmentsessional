# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('CodeTeaserApp', '0013_auto_20151206_0436'),
    ]

    operations = [
        migrations.AlterField(
            model_name='submission',
            name='submission_id',
            field=models.AutoField(default=1, primary_key=True, serialize=False),
        ),
    ]
