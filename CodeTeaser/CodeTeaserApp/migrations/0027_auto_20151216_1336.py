# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('CodeTeaserApp', '0026_auto_20151216_1316'),
    ]

    operations = [
        migrations.AlterField(
            model_name='testcase',
            name='inp',
            field=models.FileField(null=True, upload_to=''),
        ),
        migrations.AlterField(
            model_name='testcase',
            name='outp',
            field=models.FileField(null=True, upload_to=''),
        ),
    ]
