# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('CodeTeaserApp', '0029_auto_20151217_0440'),
    ]

    operations = [
        migrations.AddField(
            model_name='problemcategory',
            name='tag',
            field=models.CharField(default='', max_length=10),
        ),
    ]
