# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('CodeTeaserApp', '0010_remove_problemset_category'),
    ]

    operations = [
        migrations.AddField(
            model_name='problemset',
            name='category',
            field=models.ManyToManyField(to='CodeTeaserApp.ProblemCategory'),
        ),
    ]
