# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import CodeTeaserApp.models


class Migration(migrations.Migration):

    dependencies = [
        ('CodeTeaserApp', '0016_auto_20151214_1136'),
    ]

    operations = [
        migrations.AlterField(
            model_name='solution',
            name='code',
            field=models.FileField(upload_to=CodeTeaserApp.models.get_solution_file_Name),
        ),
    ]
