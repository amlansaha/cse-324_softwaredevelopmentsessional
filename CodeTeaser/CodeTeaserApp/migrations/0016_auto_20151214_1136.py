# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import CodeTeaserApp.models


class Migration(migrations.Migration):

    dependencies = [
        ('CodeTeaserApp', '0015_auto_20151206_0504'),
    ]

    operations = [
        migrations.AlterField(
            model_name='solution',
            name='code',
            field=models.FileField(upload_to=''),
        ),
        migrations.AlterField(
            model_name='submission',
            name='code',
            field=models.FileField(upload_to=CodeTeaserApp.models.get_submission_file_Name, null=True),
        ),
        migrations.AlterUniqueTogether(
            name='submission',
            unique_together=set([('username', 'submission_time')]),
        ),
    ]
