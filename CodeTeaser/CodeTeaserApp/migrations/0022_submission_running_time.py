# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('CodeTeaserApp', '0021_auto_20151214_1557'),
    ]

    operations = [
        migrations.AddField(
            model_name='submission',
            name='running_time',
            field=models.FloatField(default=0.0),
        ),
    ]
