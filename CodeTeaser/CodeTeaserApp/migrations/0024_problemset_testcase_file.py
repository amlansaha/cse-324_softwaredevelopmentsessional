# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import CodeTeaserApp.models


class Migration(migrations.Migration):

    dependencies = [
        ('CodeTeaserApp', '0023_auto_20151214_1636'),
    ]

    operations = [
        migrations.AddField(
            model_name='problemset',
            name='testcase_file',
            field=models.FileField(upload_to=CodeTeaserApp.models.get_testcase_filename, default=''),
            preserve_default=False,
        ),
    ]
