# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('CodeTeaserApp', '0009_auto_20151206_0128'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='problemset',
            name='category',
        ),
    ]
