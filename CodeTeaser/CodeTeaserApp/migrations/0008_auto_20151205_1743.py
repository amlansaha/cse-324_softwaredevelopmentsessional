# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('CodeTeaserApp', '0007_auto_20151205_1653'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='problemset',
            name='authors',
        ),
        migrations.AddField(
            model_name='problemset',
            name='author',
            field=models.ForeignKey(default=1, to=settings.AUTH_USER_MODEL),
        ),
    ]
