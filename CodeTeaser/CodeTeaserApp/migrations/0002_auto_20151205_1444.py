# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('CodeTeaserApp', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserType',
            fields=[
                ('utype', models.CharField(serialize=False, max_length=15, default='USR', primary_key=True)),
            ],
            options={
                'managed': True,
                'db_table': 'user_type',
            },
        ),
        migrations.AlterField(
            model_name='problemset',
            name='constrains',
            field=models.TextField(),
        ),
        migrations.AlterField(
            model_name='problemset',
            name='input_format',
            field=models.TextField(),
        ),
        migrations.AlterField(
            model_name='problemset',
            name='output_format',
            field=models.TextField(),
        ),
        migrations.AlterField(
            model_name='problemset',
            name='sample_input',
            field=models.TextField(),
        ),
        migrations.AlterField(
            model_name='problemset',
            name='sample_output',
            field=models.TextField(),
        ),
        migrations.AlterField(
            model_name='problemset',
            name='statement',
            field=models.TextField(),
        ),
        migrations.AlterField(
            model_name='problemset',
            name='title',
            field=models.CharField(max_length=40),
        ),
        migrations.AddField(
            model_name='users',
            name='user_type',
            field=models.ForeignKey(to='CodeTeaserApp.UserType', default='USR'),
        ),
    ]
