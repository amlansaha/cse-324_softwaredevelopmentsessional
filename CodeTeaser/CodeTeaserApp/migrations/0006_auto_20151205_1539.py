# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('CodeTeaserApp', '0005_auto_20151205_1504'),
    ]

    operations = [
        migrations.RenameField(
            model_name='clarification',
            old_name='problem_id',
            new_name='problem',
        ),
        migrations.RenameField(
            model_name='clarificationreply',
            old_name='problem_id',
            new_name='problem',
        ),
        migrations.RenameField(
            model_name='problemlock',
            old_name='problem_id',
            new_name='problem',
        ),
        migrations.RenameField(
            model_name='solution',
            old_name='problem_id',
            new_name='problem',
        ),
        migrations.RenameField(
            model_name='submission',
            old_name='problem_id',
            new_name='problem',
        ),
        migrations.RenameField(
            model_name='testcase',
            old_name='problem_id',
            new_name='problem',
        ),
        migrations.AlterField(
            model_name='solution',
            name='code',
            field=models.TextField(),
        ),
        migrations.AlterField(
            model_name='submission',
            name='code',
            field=models.TextField(),
        ),
        migrations.AlterUniqueTogether(
            name='problemlock',
            unique_together=set([('problem', 'username')]),
        ),
    ]
