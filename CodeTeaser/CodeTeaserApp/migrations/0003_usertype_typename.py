# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('CodeTeaserApp', '0002_auto_20151205_1444'),
    ]

    operations = [
        migrations.AddField(
            model_name='usertype',
            name='typename',
            field=models.CharField(unique=True, default='User', max_length=15),
        ),
    ]
