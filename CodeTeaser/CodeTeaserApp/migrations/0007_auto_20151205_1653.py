# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('CodeTeaserApp', '0006_auto_20151205_1539'),
    ]

    operations = [
        migrations.AlterField(
            model_name='solution',
            name='language',
            field=models.ForeignKey(to='CodeTeaserApp.Language', blank=True),
        ),
        migrations.AlterField(
            model_name='solution',
            name='problem',
            field=models.ForeignKey(to='CodeTeaserApp.ProblemSet', blank=True),
        ),
    ]
