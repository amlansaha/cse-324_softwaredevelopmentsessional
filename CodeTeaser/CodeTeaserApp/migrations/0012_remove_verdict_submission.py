# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('CodeTeaserApp', '0011_problemset_category'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='verdict',
            name='submission',
        ),
    ]
