# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import CodeTeaserApp.models


class Migration(migrations.Migration):

    dependencies = [
        ('CodeTeaserApp', '0019_auto_20151214_1234'),
    ]

    operations = [
        migrations.AlterField(
            model_name='submission',
            name='code',
            field=models.FileField(default='', upload_to=CodeTeaserApp.models.get_submission_file_Name),
            preserve_default=False,
        ),
    ]
