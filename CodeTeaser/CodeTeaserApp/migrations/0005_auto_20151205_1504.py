# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('CodeTeaserApp', '0004_auto_20151205_1456'),
    ]

    operations = [
        migrations.CreateModel(
            name='LoginLog',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('ip', models.GenericIPAddressField()),
                ('login_time', models.DateTimeField(auto_now=True)),
                ('logout_time', models.DateTimeField(null=True)),
                ('username', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'managed': True,
                'db_table': 'login_log',
            },
        ),
        migrations.AlterUniqueTogether(
            name='loginlog',
            unique_together=set([('username', 'ip', 'login_time', 'logout_time')]),
        ),
    ]
