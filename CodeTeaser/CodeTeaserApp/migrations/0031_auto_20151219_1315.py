# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('CodeTeaserApp', '0030_problemcategory_tag'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='clarificationreply',
            name='problem',
        ),
        migrations.RemoveField(
            model_name='clarificationreply',
            name='username',
        ),
        migrations.AddField(
            model_name='clarification',
            name='parent_post',
            field=models.ForeignKey(null=True, to='CodeTeaserApp.Clarification'),
        ),
        migrations.DeleteModel(
            name='ClarificationReply',
        ),
    ]
