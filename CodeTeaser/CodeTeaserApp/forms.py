from django.forms import ModelForm
from django import forms
from django.contrib import admin
from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import ReadOnlyPasswordHashField, UserCreationForm, AuthenticationForm
from django.core.validators import MaxValueValidator, MinValueValidator
from django.conf import settings
from django.core.files import File
import os
from zipfile import *
#from customauth.models import Users
from .models import *# Users,ProblemSet,TestCase
from .run_judge_code import JudgeCoderunner

class RegisterForm(UserCreationForm):
	"""A form for creating new users. Includes all the required
	fields, plus a repeated password."""
	username = forms.CharField(max_length=70, label="Username", widget=forms.TextInput(attrs={'tabindex':"1", 'class':"form-control",'placeholder':"Username"}))
	user_full_name = forms.CharField(max_length=50, label="User Full Name", widget=forms.TextInput(attrs={'tabindex':"1", 'class':"form-control",'placeholder':"Full Name"}))
	user_email = forms.EmailField(max_length=255, label="email", widget=forms.TextInput(attrs={'tabindex':"1", 'class':"form-control",'placeholder':"Email"}))
	password1 = forms.CharField(label='Password', widget=forms.PasswordInput(attrs={'tabindex':"1", 'class':"form-control",'placeholder':"Password"}))
	password2 = forms.CharField(label='Confirm Password', widget=forms.PasswordInput(attrs={'tabindex':"1", 'class':"form-control",'placeholder':"Confirm Password"}))

	class Meta:
		model = Users
		fields = ('username', 'user_full_name', 'user_email')
	
	# clean_FIELDNAME() function is used to check the validity of the FIELDNAME
	def clean_username(self):
		"""clean_FIELDNAME() function is used to check the validity of the FIELDNAME"""
		username=self.cleaned_data['username']
		if Users.objects.filter(username=username).exists():
			raise forms.ValidationError("Username already exists")
		return username
	
	def clean_email(self):
		"""clean_FIELDNAME() function is used to check the validity of the FIELDNAME"""
		email=self.cleaned_data['email']
		if Users.objects.filter(email=email).exists():
			raise forms.ValidationError("This email has already been registered")
		return email
		
	def clean_password2(self):
		"""Check that the two password entries matched or not"""
		password1 = self.cleaned_data.get("password1")
		password2 = self.cleaned_data.get("password2")
		if password1 and password2 and password1 != password2:
			raise forms.ValidationError("Passwords don't match")
		return password2

	def save(self, commit=True):
		# Save the provided password in hashed format
		user = super(UserCreationForm, self).save(commit=False)
		user.set_password(self.cleaned_data["password1"])
		if commit:
			user.save()
		return user

class PostProblemForm(ModelForm):
	"""A form for posting a new problem. This form inputs elements for ProblemSet model."""
	problem_id = forms.CharField(label='Problem ID',widget=forms.TextInput(attrs={'class':'form-control',}))
	title = forms.CharField(label='Problem Title',widget=forms.TextInput(attrs={'class':"form-control",}))
	statement = forms.CharField(label='Problem Statement',widget=forms.Textarea(attrs={'class':"form-control",}))
	constrains = forms.CharField(label='Constrains',widget=forms.Textarea(attrs={'class':"form-control",}))
	input_format = forms.CharField(label='Input',widget=forms.Textarea(attrs={'class':"form-control",}))
	output_format = forms.CharField(label='Output',widget=forms.Textarea(attrs={'class':"form-control",}))
	sample_input = forms.CharField(label='Sample Input',widget=forms.Textarea(attrs={'class':"form-control",}))
	sample_output = forms.CharField(label='Sample Output',widget=forms.Textarea(attrs={'class':"form-control",}))
	time_limit = forms.IntegerField(label='Time Limit(in miliseconds)',widget=forms.TextInput(attrs={'class':"form-control",}),
				validators = [
					MaxValueValidator(20000),
					MinValueValidator(100)	#In miliseconds
				])
	mem_limit = forms.IntegerField(label='Memory Limit(in Kilobytes)',widget=forms.TextInput(attrs={'class':"form-control",}),
				validators = [
					MaxValueValidator(262144),
					MinValueValidator(10240)	#In Kilobytes
				])
#	testcase_file = forms.FileField(label='Testcases')

	class Meta:
		model = ProblemSet
		fields = ('problem_id','title', 'statement', 'constrains','input_format','output_format','sample_input','sample_output','time_limit','mem_limit','testcase_file')

	
	def clean_problem_id(self):
		'''Checks the validity of the problem id given in the form by the problem author.'''
		pid = self.cleaned_data['problem_id']
		
		if(len(pid.split())>1 or '.' in pid or ('/' in pid) or ('\\' in pid) ):
			raise forms.ValidationError("Problem ID cannot contain any dots('.'), slashes or whitespaces")
		return pid
	
	def clean_testcase_file(self):
		'''Checks the validity of the testcase file given in the form by the problem author.'''
		data = self.clean()
		casefile=data.get('testcase_file')
		file_type = casefile.content_type.split('/')[0]
		print("printing from clean testcase file... file_type:",casefile.content_type)
		
		if ( file_type != 'text' ):
			if ( casefile.content_type != 'application/zip'):
				raise forms.ValidationError('The file content should be in plain text(*.txt) or zipped (*.zip) format.')
		
		return casefile
	
	
	def extract_testcases(self, pset):	#this method will unzip the testcases and will them properly
		'''If the provided testcase file is in zipped format, this function extracts it and saves it properly.'''
		casefile = pset.testcase_file
#		relative_path = os.path.split(casefile.name)[0]	#the path relative to MEDIA_ROOT
#		crd=os.getcwd()
		filename = os.path.join(settings.MEDIA_ROOT,casefile.name)	#filename with directory of the file
		print("extract_testcases(): ",filename)
		filepath = os.path.split(filename)[0]
#		os.chdir(filepath)
		print("From extract_testcases()... Current directory:",filepath)

		if (filename.split('.')[-1]=='zip'):
			zip_archive = ZipFile(filename)
			file_list = list(zip_archive.namelist())
			file_list.sort()
			caseno = 1
			
			for inp_file_name in file_list:	
				zip_archive.extract(inp_file_name,filepath)
				print("extracting: ",inp_file_name)
#				new_file_name = pset.problem_id+'.'+str(caseno)+'.in'
#				os.rename(inp_file, new_file_name)
				tcin = TestCase(problem=pset,caseno=caseno)
				new_file = File(open(filepath+'/'+inp_file_name))	#File() is a class of django.core.files package... it is need for new FieldFile object
				tcin.inp.save(name=inp_file_name,content=new_file)
				tcin.save()
#				dd9eafea070ae2e188d2fbe74b442a70d201bf4d
				print("File saved to: ", tcin.inp.name)
				caseno+= 1
			
		
		else:
			print("testcases are trying to be extracted")
			tcin = TestCase(problem=pset,caseno=1,inp=casefile)
			tcin.save()
			print("testcases extracted")
		
#		os.chdir(crd)
		
	def save(self, author, commit=True):
		pset = super(PostProblemForm,self).save(commit=False)
		print(self.cleaned_data['title'])
		if commit:
			if type(author)==Users:
				print("author: "+author.username)
				pset.author=author
			else:
				raise forms.ValidationError("'author' is not authorized")
			
			pset.save()
			self.extract_testcases(pset)
			print('problemset saved')
			
		return pset


class PostSolutionForm(ModelForm):
	"""A form for posting the solution of a problem of the problem setter. This form inputs elements for Solution model."""
	class Meta:
		model = Solution
		exclude=('is_valid',)
		
	def clean_code(self):
		data = self.clean()
		print("cleaned_data: ",data)
		codefile=data.get('code')
		lang_id = data.get('language')
		print("debugging from clean_code: ",codefile,lang_id)
#		lang_ext = Language.objects.get(id=int(lang_id)).ext
		file_type = codefile.content_type.split('/')[0]
		splited_filename = str(codefile).split('.')
		
		
		if(file_type != 'text'):
			raise forms.ValidationError("Invalid source code")
		elif (len(splited_filename)<2):
			raise forms.ValidationError("File name should have an extension.")
			
		return codefile
	
	
	def save(self,commit=True):
		soln = super(PostSolutionForm,self).save(commit=False)
		if commit:
			soln.save()
			JudgeCoderunner(soln)
		return soln

#class PostTestCasesForm(ModelForm):
#	class Meta:
#		model = Solution
#		exclude=()

#	def save(self, commit=True):
#		tcase = super(PostTestCaseForm,self).save(commit=False)
##		print(self.cleaned_data['title'])
#		if commit:
#			soln.save()
#			
#		return tcase


class GeneralSubmissionForm(ModelForm):
	"""A form for submitting the solution of a problem of the problem setter. This form inputs elements for Solution model."""
	problem = forms.CharField(max_length=20,label = 'Problem ID')
#	raw_code = forms.CharField(label='Code',widget=forms.Textarea(attrs={'class':"form-control",}))
	code = forms.FileField(label='Code File')
	language = forms.ModelChoiceField(queryset=Language.objects.all(),label = 'Language ')
#	def clean(self):
#		data = super(GeneralSubmissionForm,self).clean()
#		
	
	def __init__(self,*args,**kwargs):
		prob_id = ''
		if('prob_id' in kwargs):
			prob_id=kwargs['prob_id']
			del kwargs['prob_id']
		super(GeneralSubmissionForm,self).__init__(*args,**kwargs)
		self.fields['problem'].widget = widget=forms.TextInput(attrs={'value':prob_id,})
			
	
	def clean_problem(self):
		prb_id = self.cleaned_data.get('problem')
		try:
			prb = ProblemSet.objects.get(problem_id=prb_id)
		except:
			raise forms.ValidationError("Invalid Problem ID")
		return prb
	
	def clean_code(self):
#		data = super(GeneralSubmissionForm,self).clean()
		data = self.clean()
#		print("cleaned_data: ",data)
		codefile=data.get('code')
		raw_code = data.get('raw_code')
		lang_id = data.get('language')
		print("debugging from clean_code: ",codefile,lang_id)
#		lang_ext = Language.objects.get(id=int(lang_id)).ext
		file_type = codefile.content_type.split('/')[0]
		splited_filename = str(codefile).split('.')
		
		if ( codefile == None and raw_code == None ):
			raise forms.ValidationError("Either raw code or code file should be submitted.")
		
		if ( not codefile and raw_code):
			temp = open('code','w')
			temp.write(raw_code)
			codefile = File(temp)
			print("CASTING")
			
		if(file_type != 'text'):
			raise forms.ValidationError("Invalid source code")
		elif (len(splited_filename)<2):
			raise forms.ValidationError("File name should have an extension.")
			
		return codefile

	def clean(self):
		data = super(GeneralSubmissionForm,self).clean()
#		data = self.clean()
#		print("cleaned_data: ",data)
		codefile=data.get('code')
		raw_code = data.get('raw_code')
		lang_id = data.get('language')
		print("debugging from clean(): ",codefile,raw_code)
#		lang_ext = Language.objects.get(id=int(lang_id)).ext
		
		if ( codefile == None and raw_code == None ):
			print("INVALID UP")
			raise forms.ValidationError("Either raw code or code file should be submitted.")
		
		print("QQQQQ: ",type(codefile))
		if ( not codefile and raw_code):
			temp = open('code','w')
			temp.write(raw_code)
			codefile = File(temp)
			print("CASTING")
		
		else:
			data['raw_code'] = 'NOTHING'
		file_type = codefile.content_type.split('/')[0]
		splited_filename = str(codefile).split('.')

		if(file_type != 'text'):
			raise forms.ValidationError("Invalid source code")
		elif (len(splited_filename)<2):
			raise forms.ValidationError("File name should have an extension.")
		
		
#		data['code'] = codefile
		return data
	
		
	def save(self,user,commit=True):
		submsn = super(GeneralSubmissionForm,self).save(commit=False)
		verdict = Verdict(verd=VerdictType.objects.get(vtype='IQ'))
		verdict.save()
		submsn.verdict=verdict


		if(commit):
#			submsn.verdict.save()
			submsn.save()
			
		return submsn
		
	class Meta:
		model = Submission
		fields = ('problem','code','language')
#		exclude=('username','submission_time','verdict')
		
		
		
class AddCityForm(ModelForm):
	country = forms.ModelChoiceField(queryset=Country.objects.all(),label = 'Country Name')
	
	class Meta:
		model = City
		exclude=()

		
class AddCountryForm(ModelForm):
	name = forms.CharField(label = 'Country Name')
	
	class Meta:
		model = Country
		exclude=()

class AddLanguageForm(ModelForm):
	
	class Meta:
		model = Language
		exclude=()

