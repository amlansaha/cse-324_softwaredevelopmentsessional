from __future__ import unicode_literals
from django.db import models
from django.conf import settings
from datetime import datetime
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser
)
from django.core.validators import RegexValidator
from time import time
import os
#from .runcode import Coderunner
# Create your models here.

class MyUserManager(BaseUserManager):
    def create_user(self, username=None, password=None, user_email=None, user_full_name=None):
        """
        Creates and saves a User with the given email, date of
        birth and password.
        """
        if not user_email:
            raise ValueError('Users must have an email address')

        user = self.model(
        	username=username,
            user_email=self.normalize_email(user_email),
            user_full_name = user_full_name,
#            user_image = user_image,
        )
        user.user_type = UserType(utype="USR")
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username, password, user_email, user_full_name):
        """
        Creates and saves a superuser with the given email, date of
        birth and password.
        """
        user = self.create_user(username=username,
            password=password,
            user_email=user_email,
            user_full_name=user_full_name,
#            user_image = user_image
        )
        user.is_admin = True
        user.user_type = UserType(utype="ADM")
        user.save(using=self._db)
        return user
        
class Users(AbstractBaseUser):
	'''User model of the system'''
	username = models.CharField(unique=True, max_length=20)
	user_full_name = models.CharField(max_length=70)
	user_email = models.EmailField(
			verbose_name='Email Address',
			max_length=255,
			unique=True,
		)
	date_joined = models.DateField(default=datetime.now,)
	user_type = models.ForeignKey('UserType',default='USR')
	is_active = models.BooleanField(default=True)
	is_admin = models.BooleanField(default=False)
	
	objects = MyUserManager()
	
	institution = models.ForeignKey('Institution',null=True)
	city = models.ForeignKey('City',null=True)
#	profile_pic = models.ImageField(upload_to='Photos')
	
	USERNAME_FIELD = 'username'
	REQUIRED_FIELDS = ['user_full_name','user_email']
	
	
	def __str__(self):
		return self.username
		
	def get_full_name(self):
		# The user is identified by their email address
		return self.user_full_name
	
	def get_short_name(self):
		# The user is identified by their email address
		return self.user_full_name.split()[0]

	def __str__(self):			  # __unicode__ on Python 2
		return self.username

	def has_post_prob_perm(self):
		"Does the user have problem posting permission?"
		
		ret = (self.user_type.utype == 'ADM' or self.user_type.utype == 'PST')
		
		if(ret):
			print("user "+self.username+" has posting permission")
		else:
			print("No permission")
		return ret
		
	def has_module_perms(self, app_label):
		"""Does the user have permissions to view the app `app_label`?"""
		# Simplest possible answer: Yes, always
		return True
		
	@property
	def is_staff(self):
		"""Is the user a member of staff?"""
		# Simplest possible answer: All admins are staff
		return self.is_admin 
		
	class Meta:
		managed = True
		db_table = 'users'


class UserType(models.Model):
	ADM = 'ADM'
	USR = 'USR'
	PST = 'PST'
#	FSV = 'FSV'
	
#	choice = (
#		(ADM,'Admin'),
#		(USR,'User'),
#		(PST,'Problem Setter'),
##		(FSV,'Forum Supervisor'),
#	)
	utype = models.CharField(max_length=15,default='USR',primary_key=True)
	typename= models.CharField(max_length=15, unique=True, null=False, default='User')
	
	def is_admin(self):
		"""checks if the user is admin or not"""
		return self.utype == self.ADM
	class Meta:
		managed = True
		db_table = 'user_type'


class Language(models.Model):
#	alphanumeric = RegexValidator(r'^[0-9a-zA-Z\*]*$', 'Only alphanumeric characters are allowed.')
	
	lang = models.CharField(max_length=15,null=False,unique=True)
	compiler = models.CharField(max_length=16)	#compiler or owner/developer's name, like g++ for c++
	version = models.CharField(max_length=10)
	ext = models.CharField(max_length=4,null=False)
	
	def __str__(self):
		
		cmpl = ''
		if(self.lang=='C++'):
			cmpl = self.compiler+' '
		return self.lang+" ("+cmpl+'Version: '+self.version+")"
	
	class Meta:
		managed = True
		db_table = 'language'
	
#def get_problemset_testcase_filename(instance, filename):
#	ext = filename.split('.')[-1]
#	if(ext.lower() != 'zip'):
#		ext = 'in'
#	return '/'+instance.problem_id+'/'+instance.problem_id+'.'+ext

def get_testcase_filename(instance, filename):
	ext = filename.split('.')[-1]
	if(ext.lower() != 'zip'):
		ext = 'in'
	return 'Inputs/'+instance.problem_id+'/'+instance.problem_id+'.'+ext	
	
class ProblemSet(models.Model):
	problem_id = models.CharField(max_length=12, null=False, primary_key=True)
	title = models.CharField(max_length=40,null=False)
	statement = models.TextField(null=False)
	constrains = models.TextField()
	input_format = models.TextField(null=False)
	output_format = models.TextField(null=False)
	sample_input = models.TextField(null=False)
	sample_output = models.TextField(null=False)
	time_limit = models.IntegerField(null=False,default=2000)			#In miliseconds
	mem_limit = models.IntegerField(null=False,default=65326)
	testcase_file = models.FileField(upload_to=get_testcase_filename,null=False)
	is_published = models.BooleanField(null=False,default=False)
	author = models.ForeignKey(Users,null=False,default=1)
	category = models.ManyToManyField('ProblemCategory')
	
	def __str__(self):
		return self.problem_id
	
	class Meta:
		managed = True
		db_table = 'problemset'

class ProblemCategory(models.Model):
	category_title = models.CharField(max_length=20,null=False)
	tag = models.CharField(max_length=10,null=False,default='')
	class Meta:
		managed = True
		db_table = 'problem_category'
	

def get_solution_file_Name(instance,filename):
	"""retuns filename wrt filepath"""
	fname = "Solutions/%s/%s_%s.%s" % (instance.problem,instance.problem,filename,instance.language.ext)
	return fname
	
class Solution(models.Model):
	problem = models.ForeignKey(ProblemSet,blank=True)
	language = models.ForeignKey(Language,blank=True)
	code = models.FileField(upload_to=get_solution_file_Name,null=False)
	is_valid = models.BooleanField(default = False)
	class Meta:
		managed = True
		db_table = 'solution'
		unique_together=('problem','language')

def get_input_file_name(instance,filename):
	"""retuns the filename of input testcase"""
	problem_id = instance.problem.problem_id
	caseno = instance.caseno
	fname=os.path.join('Inputs',problem_id,problem_id+'.'+str(caseno)+'.in')
	return fname

def get_output_file_name(instance,filename):
	"""retuns the filename of output file of the testcases"""
	problem_id = instance.problem.problem_id
	caseno = instance.caseno
	fname=os.path.join('Outputs',problem_id,problem_id+'.'+str(caseno)+'.out')
	return fname
	
class TestCase(models.Model):
	problem = models.ForeignKey(ProblemSet)
	caseno = models.IntegerField(default=1)
	inp = models.FileField(upload_to = get_input_file_name,null=True)
	outp = models.FileField(upload_to=get_output_file_name,null=True)
	
	class Meta:
		managed = True
		db_table = 'testcase'
		unique_together=('problem','caseno')

class VerdictType(models.Model):
	AC = 1
	CE = 2
	MLE = 3
	OLE = 4
	PE = 6
	RN = 7
	RE = 8
	SF = 9
	TLE = 10
	IQ = 11
	WA = 12
	CMP = 'CMP'
	
	verd_choice = (
		(AC,'Accepted'),
		(TLE,'Time Limit Exceeded'),
		(WA,'Wrong Answer'),
		(RE,'Runtime Error'),
		(CE,'Compilation Error'),
		(PE,'Presentation Error'),
		(MLE,'Memory Limit Exceeded'),
		(OLE,'Output Limit Exceeded'),
		(IQ, 'In Queue'),
		(SF, 'Submission Failed'),
		(RN, 'Running')
	)
	
	vtype = models.CharField(max_length=3,primary_key=True)
	vmsg = models.CharField(max_length=22,unique=True)
	
	def __str__(self):
		return self.vmsg
	
	class Meta:
		managed=True
		db_table='verdict_type'
		unique_together=('vtype','vmsg')


class Verdict(models.Model):
	verd = models.ForeignKey(VerdictType, default='IQ')
	running_time = models.FloatField(default=0.0)								
	caseno = models.IntegerField(default=1)
	msg = models.CharField(max_length=30)
#	submission = models.ForeignKey("Submission",null=True,related_name='submission_id_for_verdict_table')
	
	class Meta:
		managed = True
		db_table = 'verdict'


def get_submission_file_Name(instance,filename):
	"""returns the filename of submissions wrt filepath"""
	fname = "Submissions/%s_%s/%s_%s_" % (instance.username,instance.submission_time,instance.username,instance.submission_time)
	fname=fname.replace('.','_')
	fname=fname.replace(' ','_')
	return fname+filename
	
class Submission(models.Model):
	submission_id = models.AutoField(primary_key=True)
	username = models.ForeignKey(Users)
	problem = models.ForeignKey(ProblemSet)
	submission_time = models.DateTimeField(auto_now=True,editable=False)
	code = models.FileField(upload_to=get_submission_file_Name, null=False,default='')
	language = models.ForeignKey(Language)
	verdict = models.OneToOneField(Verdict,related_name='verdict_id_for_submission_table')
        
	def setVerd(self,verd,caseno=0,running_time=0):
		"""set verdict for solutions"""
		self.verdict.verd = verd
		self.verdict.caseno=caseno
		self.running_time = running_time
		self.verdict.save(force_update=True)
#		self.save(force)
		
#	def save(self,commit=True):
#		super(Submission,self).save(commit)
#		if(self.verdict.verd.vtype=='IQ'):
#			from .runcode import Coderunner
#			Coderunner(self)
		
	class Meta:
		managed = True
		db_table = 'submission'
		unique_together=('username','submission_time')

class LoginLog(models.Model):
	"""stored the login info of users """
	username=models.ForeignKey(Users)
	ip = models.GenericIPAddressField()
	login_time = models.DateTimeField(auto_now=True)
	logout_time = models.DateTimeField(null=True)
	
	class Meta:
		managed = True
		db_table = 'login_log'
		unique_together = ('username','ip','login_time','logout_time')
		
class Clarification(models.Model):
	username = models.ForeignKey(Users)
	problem = models.ForeignKey(ProblemSet)
	ask_time = models.DateTimeField(auto_now=True)
	parent_post = models.ForeignKey('Clarification',null=True)
	
	class Meta:
		managed = True
		db_table = 'clarification'


class ProblemLock(models.Model):
	problem = models.ForeignKey(ProblemSet)
	username = models.ForeignKey(Users)
	
	class Meta:
		managed = True
		db_table = 'problem_lock'
		unique_together = ('problem','username')
		
		
class Country(models.Model):
	name = models.CharField(max_length=20,null=False,unique=True)
	
	def __str__(self):
		return self.name
	
	class Meta:
		managed = True
		db_table = 'country'
		
class City(models.Model):
	city = models.CharField(max_length=20,null=False)
	country = models.ForeignKey(Country,null=False)
	
	def __str__(self):
		return city+', '+country.name
	class Meta:
		managed = True
		db_table = 'city'
		unique_together = ('city','country')
		
class Institution(models.Model):
	name = models.CharField(max_length=40)
	city = models.ForeignKey(City)

	class Meta:
		managed = True
		db_table = 'institution'
		unique_together = ('name','city')

