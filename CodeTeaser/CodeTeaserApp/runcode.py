#import models
import os,time
print(os.getcwd())
from CodeTeaserApp.models import *
from django.conf import settings

import subprocess
from sys import argv

#call(["head","-10","Temporary.cpp","|","tail","-2","Temporary.cpp"])

class Coderunner(object):
	MEDIA_ROOT = ""
	filepath=''
	filedir=''
	filename = ''
	vtype = {}
	submsn = None
	
	def __init__(self,submsn):
		crd=os.getcwd()

		self.MEDIA_ROOT = settings.MEDIA_ROOT
		self.filepath = os.path.join(self.MEDIA_ROOT,submsn.code.name)
	#	print(filepath)
		self.filedir = os.path.dirname(self.filepath)
		self.filename = os.path.split(self.filepath)[-1]
		self.submsn = submsn
		vs = VerdictType.objects.all()
		for v in vs:
			self.vtype[v.vtype] = v
#			print("kkkkk: ",v.vtype)
		
		os.chdir(self.filedir)
		
		self.templist = self.filename.split('.')
		self.file_domain = self.templist[0]
		
		compile_status = self.compile_code(submsn)
		if ( compile_status != 0 ):
			return

		self.run_code(submsn)
	
		os.chdir(crd)

	
	def compile_code(self,submsn):
		self.compile_log_file = open('cmplog.'+self.file_domain,'w')
		self.error_file = open('err.'+self.file_domain,'w')
		print("Compilling...",submsn.submission_id)
		submsn.setVerd(self.vtype["CMP"])
		submsn.save()
		
	
		compile_status = 1
		
		if submsn.language.lang == 'C':
			compile_status = self.compile_c_code()
		elif submsn.language.lang == 'C++':
			compile_status = self.compile_cpp_code()
		elif submsn.language.lang == 'C++11':
			compile_status = self.compile_cpp11_code()
		elif submsn.language.lang.lower() == 'java':
			compile_status = self.compile_java_code()

		self.compile_log_file.close()
		self.error_file.close()

#		os.remove('cmplog.'+self.file_domain)
#		os.remove('err.'+self.file_domain)
		
		print("Compiled...status: ",compile_status) 
		if ( compile_status != 0 ):
			submsn.setVerd(self.vtype["CE"])
			submsn.save()
		
		return compile_status
	
	
	def compile_c_code(self):
		compile_status = subprocess.call("gcc "+self.filename+" -w -std=c99 -o "+self.filedir+'/a.out',stdout=self.compile_log_file,stdin=None,shell=True,stderr=self.error_file)
		return compile_status
	
	def compile_cpp_code(self):
		compile_status = subprocess.call("g++ "+self.filename+" -w -std=c99 -o "+self.filedir+'/a.out',stdout=self.compile_log_file,stdin=None,shell=True,stderr=self.error_file)
		return compile_status
	
	def compile_cpp11_code(self):
		compile_status = subprocess.call("g++ "+self.filename+" -w -std=c++11 -o "+self.filedir+'/a.out',stdout=self.compile_log_file,stdin=None,shell=True,stderr=self.error_file)
		return compile_status
	
	def compile_java_code(self):
		compile_status = subprocess.call("javac -d "+self.filedir+' '+self.filedir+'/'+self.filename,stdout=self.compile_log_file,stdin=None,shell=True,stderr=self.error_file)
#		print("java compile status: ",compile_status)
		return compile_status
		
		
		
		
	def run_code(self,submsn):
		ts = 0
		te = -1
		running_time = 0.0
		
#		submsn.save()
		
		problem_id = submsn.problem.problem_id
		
		testcase_inputdir = os.path.join(self.MEDIA_ROOT,'Inputs',problem_id)
		testcase_outdir = os.path.join(self.MEDIA_ROOT,'Outputs',problem_id)
		
		testcases = TestCase.objects.filter(problem=problem_id)
		caseno=0
		
		for case in testcases:
#			input_file = open(os.path.join(testcase_inputdir,problem_id+"."+str(caseno)+".in"),'r')
#			original_output = os.path.join(testcase_outdir,problem_id+"."+str(caseno)+".out")
			input_file = case.inp
			original_output = case.outp
			caseno = case.caseno
			
			submsn.setVerd(self.vtype["RN"],caseno=caseno)
			submsn.save()
								
			output_file_name = self.filedir+self.file_domain+"."+str(caseno)+".out"
			output_file = open(output_file_name,'w')
			
			print("outdir: ",testcase_outdir, " output ", original_output)
			ts = time()
			time_limit = submsn.problem.time_limit/1000.0	#Time limit is saved as miliseconds in the database. Here we have to convert it to seconds.
			running_status = ''
			
			if submsn.language.lang == 'C' or submsn.language.lang == 'C++' or submsn.language.lang == 'C++11':
				running_status = self.run_c_binary_file(inp = input_file,outp = output_file,time_limit=time_limit)
			elif submsn.language.lang.lower() == 'java':
				running_status = self.run_java_class(inp = input_file,outp = output_file,time_limit=time_limit*3)
				
			te = time()
			running_time = max(running_time,te-ts)
			
			input_file.close()
			output_file.close()
			if (running_status != 'OK' ):
				submsn.setVerd(self.vtype[running_status],caseno=caseno,running_time=running_time)	#RUNTIME ERROR
				submsn.save()
				print("ERROR...running_time:",running_time)
				break
			else:
				out_diff = open('output_difference_'+self.file_domain,'w')
				original_output_filepath = original_output.file.name
				
				check = subprocess.call('diff '+original_output_filepath + ' '+output_file_name, stdout = out_diff,shell=True)

				if ( check != 0):
					submsn.setVerd(self.vtype["WA"],caseno=caseno,running_time=running_time)
					break
				out_diff.close()
				os.remove('output_difference_'+self.file_domain)

		
		if ( submsn.verdict.verd.vtype=='RN'):	#if still the program is running, that means it must have passed all the cases
			submsn.setVerd(self.vtype['AC'],running_time=running_time)
		
		submsn.verdict.running_time = running_time
		submsn.save()
		print("Filename:",self.filename)
		print("Total running time: ",running_time)
		print("submsn.verdict.verd:",submsn.verdict.verd,"at caseno:",caseno,'\n')
	

	def run_c_binary_file(self,inp,outp,time_limit):
		try:
#			ts = time()
			running_status = running_status = subprocess.call(self.filedir+"/a.out",stdin=inp,stdout=outp,timeout=time_limit)
#			te = time()
			
			if(running_status<0):
				return 'RE'
			
			return 'OK'	
		except subprocess.TimeoutExpired as ex:
			return 'TLE'
		
	def run_java_class(self,inp,outp,time_limit):
		try:
			print("JJAAVVAA ","java -classpath "+self.filedir+" Main")
			running_status = running_status = subprocess.call("java -classpath "+self.filedir+" Main",shell=True,stdin=inp,stdout=outp,timeout=time_limit)
#			te = time()
			
			if(running_status==0):
				return 'OK'
			
			return 'RE'	
		except subprocess.TimeoutExpired as ex:
			return 'TLE'		
