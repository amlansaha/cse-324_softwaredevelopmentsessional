from django.shortcuts import render
from django.conf import settings
from django.http import HttpResponse
from django.template import RequestContext, loader
from .models import * #from .models import Locations, Districts, Hotels, Adjacency, Guides, Images, images_guide, images_location, images_user, manage, restaurants, review, transports, travel, users
from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect
from django.contrib import auth
from django.core.context_processors import csrf
from .forms import *#RegisterForm, 
from django.contrib.auth import authenticate, login, logout
from django.db import connection
from .runcode import Coderunner
from django.db import connection
# Create your views here.

def home(request):
	"""If logged in Redirect to dashboard else redirect to default home"""
	if (request.user.is_authenticated()):
		return HttpResponseRedirect("/algo/dashboard")
	template = loader.get_template('CodeTeaser/index.html')
	context = RequestContext(request)
	return HttpResponse(template.render(context))


def user_register(request):
	"""If registration form info is valid then successfully registered else invalid registration"""
	regform = RegisterForm(request.POST or None)
	
	registered = False
	alert = ""
	if regform.is_valid():
		save_it = regform.save(commit=False)
		save_it.save()
		registered = True
		alert = "Registered successfully."
		username = request.POST.get('username')
		password = request.POST.get('password1')
		print("username: "+str(username))#+" pw:"+str(password))
		
		user = authenticate(username=username,password=password)
		login(request,user)
		logg = LoginLog(username=user,ip=request.META['REMOTE_ADDR'])
		logg.save()
		return HttpResponseRedirect("/algo")
#		return render(request, 'CodeTeaser/problem_category.html',{})
#		authenticate(username=username,password=password)
		
	else:
		print ("INVALID FORM")
	return render_to_response("CodeTeaser/register.html", 
							locals(), 
							context_instance=RequestContext(request))

def user_login(request,redir=''):	#redir is the next redirecting URL
	"""If user_login is valid redirect to user dashboard or show invalid login"""
	if(redir==None):
		redir=''
	print("trying to2 login/"+redir)
	
	if(request.user.is_authenticated()):
		print("user is authenticated, username: "+request.user.username)
#		redir = '/algo'
#		print("offset: "+offset+" lkasdjf")
#		if(offset!=None):
#			redir = redir+'/'+offset
		return HttpResponseRedirect('/algo/'+redir)
	else:
		print("User is not authenticated")		
		
	if (request.method=='POST' ):
		username = request.POST['username']
		password = request.POST['password']
		
		user = authenticate(username=username,password=password)
		if user is not None:
			if user.is_active:
				login(request, user)
				print("User is valid, active and authenticated")
				print("user: %r, pw: %r"%(username, password))
				logg = LoginLog(username=user,ip=request.META['REMOTE_ADDR'])
				logg.save()
				print("loggin from ip: "+logg.ip)
				# Redirect to a success page.
				if(redir==None):
					redir=''
				print("redirecting to: "+redir)
				return HttpResponseRedirect("/algo/"+redir)
#				return HttpResponseRedirect("/algo")
			else:
				print("The password is valid, but the account has been disabled")
				# Return a 'disabled account' error message
		else:
			print("The username and password were incorrect")
			# Return an 'invalid login' error message.
	
#	regform = RegisterForm(request.POST or None)
	print("exiting login function")
	return render(request, 'CodeTeaser/login.html')

def user_logout(request):
	"""redirect to Default page"""
	logout(request)
	return HttpResponseRedirect("/algo")
	
	

def getScore(person):
	'''retuns the number of problems solved by the person'''
	cursor = connection.cursor()
	cursor.execute("SELECT count(DISTINCT problem_id) from submission s, verdict v where v.verd_id = 'AC' and v.id=s.verdict_id and s.username_id = %s;"%(person.id))
	solve_count = cursor.fetchone()[0]
	print("SSS: ",solve_count,person)
	return solve_count	
	

def getAttempt(person):
	'''retuns the number of problems attempted by the person'''
	cursor = connection.cursor()
	cursor.execute("SELECT count(DISTINCT problem_id) from submission s, verdict v where v.id=s.verdict_id and s.username_id = %s;"%(person.id))
	count = cursor.fetchone()[0]
	return count		
	
def dashboard(request):
""""""
	if (request.user.is_authenticated()):
		return HttpResponseRedirect('problems')
#		return render(request, 'CodeTeaser/problem_category.html',{})
#		return HttpResponseRedirect('/algo/problems')
	return HttpResponseRedirect("/algo/")

def post_problem(request):
	"""system stored problems  given by the admin"""
	if (request.user.is_authenticated()==False):
		return HttpResponseRedirect("/algo/login/post_problem")
	
	user = Users.objects.get(username = request.user.username)
	if(not user.has_post_prob_perm()):
		print('not enough permission')
		#to be implemented: JS warning
		return HttpResponseRedirect('/algo/dashboard')

	psetform = PostProblemForm(None)
	if(request.POST or request.FILES):
		psetform = PostProblemForm(request.POST, request.FILES)
		
#	if (request.method=='POST'):
	if(psetform.is_valid()):
		print(request.user)
		pset = psetform.save(author=user,commit=True)
		print("problemset saved by "+user.username)
		return HttpResponseRedirect("/algo/post_solution")
	else:
		print("invalid post problem form")
#	template = loader.get_template('CodeTeaser/post_problem.html')
#	context = RequestContext(request)
	
	return render_to_response("CodeTeaser/post_problem.html", 
							locals(), 
							context_instance=RequestContext(request))
	return HttpResponse(template.render(context))
	
#
def post_solution(request):
	"""system stored the given problems'  solutions with respect to problem_id posted by problem setter"""
	if (request.user.is_authenticated()==False):
		return HttpResponseRedirect("/algo/login/post_solution")
	
	user = Users.objects.get(username = request.user.username)
	if(not user.has_post_prob_perm()):
		print('not enough permission')
		#to be implemented: JS warning
		return HttpResponseRedirect('/algo/dashboard')

	solnform = PostSolutionForm()
	if(request.POST):
		solnform = PostSolutionForm(request.POST,request.FILES)
#	if(not request.POST):
#		solnform.add_prob_queryset(ProblemSet.objects.get(user))
	
	if(solnform.is_valid()):
	"""checks the validity of the solutions of  problems posted by the problem setter"""
		soln = solnform.save(commit=True)
		print("solution for problem: "+soln.problem.problem_id+" has been saved")
		return HttpResponseRedirect("/algo")
	else:
		print("invalid post problem form")
		solnform.fields['problem'].queryset = ProblemSet.objects.filter(author=user).order_by('problem_id')
#		for x in solnform.fields['problem'].queryset:
#			print("QS: "+x)
#	template = loader.get_template('CodeTeaser/post_problem.html')
#	context = RequestContext(request)
	
	return render_to_response("CodeTeaser/post_solution.html", 
							locals(), 
							context_instance=RequestContext(request))
	
def show_problems(request,category_tag=None,pageno=''):
	#categories to be implemented
	"""retuns the views of the problemset to the users"""
	if(not request.user.is_authenticated()):
		return HttpResponseRedirect('/algo')
	
	print("category: ",category_tag," pageno: ",pageno)
	if(pageno != None):
		pageno = int(pageno)
	else:
		pageno = 1
		
	if(category_tag):
		try:
			cat = ProblemCategory.objects.get(tag=category_tag)
		except:
			cat=None
		if(cat):
			problems = ProblemSet.objects.filter(category=cat)[50*(pageno-1):50*pageno-1]
		else:
			problems = ProblemSet.objects.all()[50*(pageno-1):50*pageno-1]			
	else:
		problems = ProblemSet.objects.all()[50*(pageno-1):50*pageno-1]
	
	ac_verdicts = Verdict.objects.filter(verd_id='AC')
	
	problems = list(problems)
	
	for i in range(len(problems)):
#		solve_count = Submission.objects.filter(problem = problems[i])
		cursor = connection.cursor()
		cursor.execute("SELECT count(DISTINCT username_id) from submission s, verdict v where v.verd_id = 'AC' and v.id=s.submission_id and s.problem_id = '%s';"%(problems[i].problem_id))
		solver_count = cursor.fetchone()[0]
		
		#count the number of users tried to solve the problem
		cursor.execute("SELECT count(DISTINCT username_id) from submission s, verdict v where v.id=s.submission_id and s.problem_id = '%s';"%(problems[i].problem_id))
		tried_by = cursor.fetchone()[0]
		problems[i] = [problems[i],solver_count,tried_by]
#		print("Solver_count",solver_count)
	
	return render_to_response("CodeTeaser/browse_problems.html", 
							locals(), 
							context_instance=RequestContext(request))

def show_categories(request):
	"""show the problem in differnt categories"""
	if(not request.user.is_authenticated()):
		return HttpResponseRedirect('/algo')
	
	return render_to_response("CodeTeaser/problem_category.html",
							locals(),
							context_instance=RequestContext(request))

def show_problemset(request,prob_id):
	if(not request.user.is_authenticated()):
		return HttpResponseRedirect('/algo')
	
	if(prob_id == None):
		return HttpResponseRedirect('/algo/problems')
	
	if( not ProblemSet.objects.filter(problem_id=prob_id).exists()):
		return HttpResponseRedirect('/algo/problems')
	
	pset = ProblemSet.objects.get(problem_id=prob_id)
	
	my_submissions = Submission.objects.filter(problem=pset,username=request.user).order_by('-submission_time')
	
#	problemset = ProblemSet.objects.get(prob_id)
#	if(not problemset):
#		return HttpResponseRedirect('/algo')
	
	return render_to_response("CodeTeaser/problemset.html", 
							locals(), 
							context_instance=RequestContext(request))

#View controller for solution submission page
def submit_soln(request,prob_id=''):
	"""	take the submission from the user"""
	if(not request.user.is_authenticated()):
		return HttpResponseRedirect('/algo/login')
		
	user = request.user
	if(prob_id==None):
		prob_id=''
	if(not user.is_authenticated):
		return HttpResponseRedirect('/algo/')#login/submit/'+prob_id)
	user = Users.objects.get(username = request.user.username)

	
	submitform = GeneralSubmissionForm(prob_id=prob_id)
	
	print('debug from submit_soln: ',request.POST)
	
	if(request.POST or request.FILES):
		submitform = GeneralSubmissionForm(request.POST,request.FILES)
	
		
#	print(submitform)
		
	if(submitform.is_valid()):
		submsn = submitform.save(user=user,commit=False)
		
		print("solution for problem: "+submsn.problem.problem_id+" has been saved")
		submsn.username = user
		submsn.save()		

		Coderunner(submsn)

		return HttpResponseRedirect("/algo/submissions")
		
	else:
		print("invalid submit solution form")
		print('invalid debug from submit_soln: ',request.POST)
#		submitform.fields['problem'].queryset = ProblemSet.objects.filter(author=user).order_by('problem_id')
#		for x in submitform.fields['problem'].queryset:
#			print("QS: "+x)
#	template = loader.get_template('CodeTeaser/post_problem.html')
#	context = RequestContext(request)
	
	return render_to_response("CodeTeaser/submit_solution.html", 
							locals(), 
							context_instance=RequestContext(request))
	
def view_submissions(request,username='',pageno=''):
	"""show the submission status of all_users"""
	if(not request.user.is_authenticated()):
		return HttpResponseRedirect('/algo/login')
	
	if(pageno == None):
		pageno = 1
	else:
		pageno = int(pageno)
		
	sbmsns = None
	if(username == None or username == ''):
		username = 'my'
	
	print("SUBMISSION... page,",username,pageno)
	view_all=False
	
	if(username.lower()=='all'):
		sbmsns = Submission.objects.all().order_by('-submission_time')[50*(pageno-1):50*pageno]
		view_all = True
		
	else:
		if(username.lower() != 'my'):
			user = Users.objects.get(username=username)
		else:
			user = Users.objects.get(username=request.user.username)
			
		sbmsns = Submission.objects.filter(username = user).order_by('-submission_time')[50*(pageno-1):50*pageno]

	return render_to_response("CodeTeaser/submissions.html", 
							locals(), 
							context_instance=RequestContext(request))
	
	
def show_ranklist(request,prob_id=''):
	"""show the ranks and statistics of users"""
	if(not request.user.is_authenticated()):
		return HttpResponseRedirect('/algo')
	
	all_users = list(Users.objects.all())
	all_users.sort(key=getScore,reverse=True)
	
	rank = []
	
	for i in range(len(all_users)):
		rank.append([i+1,all_users[i],getScore(all_users[i]),getAttempt(all_users[i])])
	
	return render_to_response("CodeTeaser/ranklist.html", 
							locals(), 
							context_instance=RequestContext(request))
	
	
def add_country(request):
	cntry = AddCountryForm(request.POST or None)
	print(cntry)
	if(cntry.is_valid()):
		c = cntry.save(commit=True)
		if(c!=None):
			print("country "+c.name+" added")
			HttpResponseRedirect('algo/add_country')
	
	return render_to_response("CodeTeaser/add_country.html", 
							locals(), 
							context_instance=RequestContext(request))
	
def add_city(request):
	ct = AddCityForm(request.POST or None)
	
	if(ct.is_valid()):
		ct.save(commit=True)
	
	return render_to_response("CodeTeaser/add_city.html", 
							locals(), 
							context_instance=RequestContext(request))
							
def add_institution(request):
	return HttpResponseRedirect('/algo/dashboard')
	
def add_language(request):
	if (not request.user.is_authenticated() or user.username != 'admin'):
		return HttpResponseRedirect("/algo/dashboard")
	lang = AddLanguageForm(request.POST or None)
	
	if(lang.is_valid()):
		lang.save(commit=True)
	
	return render_to_response("CodeTeaser/add_language.html", 
							locals(), 
							context_instance=RequestContext(request))

